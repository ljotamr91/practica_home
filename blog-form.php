<?php
	include("head.php"); 
	include("header.php"); 
?>

<div class="descripcion offset-1 col-7">	
	<h1>BLOG</h1><br>

	<!-- <form class="admin-form" id="blog-form" action="blogRequest.php"> -->
	<form class='admin-form' id='blog-form' action='app\request\BlogRequest.php'>

		<div class="errors-container hidden">
			<ul class="errors"></ul>
		</div>

		<div class="success-container hidden">
			<h1 class="success"></h1>
		</div>

		<div class="form-group">
			<label for="nombre">  </label>
			<input type="text" class="form-control" id="title"  name="title" placeholder="Escriba un Titulo">
		</div>

		<textarea name="comentario" id="ckeditor" rows="10" cols="80">
		</textarea>	

		<select name="option" class="col-lg-2 col-sm-12 select2">
			<option value="publico">Publico (por defecto)</option>
			<option value="privado">Privado/Oculto</option>
		</select>
					
		<br><br><input type="submit" id="enviar" class="btn btn-secundary details botones slider-btn"/>
	</form>
</div>						

<?php
	include("footer.php"); 
	include("scripts.php");
?> 

