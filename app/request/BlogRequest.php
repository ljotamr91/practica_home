<?php
    require_once ("C:/wamp/www/soluciones-informaticas/app/controller/BlogController.php");
    require_once ("C:/wamp/www/soluciones-informaticas/core/Validation.php");


    $validation = new Validation();
    $validation->isEmpty($_POST['title'], 'title', 'title');
    $validation->isEmpty($_POST['comentario'], 'comentario', 'comentario');
    $validation->isEmpty($_POST['option'], 'option', 'option');

    $validation->minLength($_POST['title'], 5, 'title', 'title');
    $validation->maxLength($_POST['title'], 21, 'title', 'title');
    // $validation->minLength($_POST['comentario'], 5, 'comentario', 'comentario');
    // $validation->maxLength($_POST['comentario'], 255, 'comentario', 'comentario');

   

    if($validation->getValidation()){
        $blog = new BlogController();
        $response['_validation'] = $validation->getValidation();
        $response['message'] = $blog->createBlog($_POST);
        echo json_encode($response);
    }else{
        echo json_encode($validation->getErrors());
    }
?>
