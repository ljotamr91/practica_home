<?php

    require_once "C:/wamp/www/soluciones-informaticas/app/controller/UserController.php";
    require_once($_SERVER['DOCUMENT_ROOT'] . "/soluciones-informaticas/core/Validation.php");
    /*$value='name' . $input_id = "name" $input_label = lo que se añade al cmapo nombre*/

    if($_POST["action"] == "create"){
        $validation = new Validation();// construir objeto en una variable, siempre arranca el constructor por si solo.
        
        //valida si esta vacio
        $validation->isEmpty($_POST['name'], 'name', 'nombre');
        $validation->isEmpty($_POST['lastname'], 'lastname', 'apellido');
        $validation->isEmpty($_POST['email'], 'email', 'email');

        //valida el minimo y maximo de caracteres de los siguientes campos.
        $validation->minLength($_POST['name'], 2, 'name', 'nombre');
        $validation->maxLength($_POST['name'], 21, 'name', 'nombre');
        $validation->minLength($_POST['lastname'], 2, 'lastname', 'apellido');
        $validation->maxLength($_POST['lastname'], 21, 'lastname', 'apellido');
        $validation->minLength($_POST['email'], 5, 'email', 'email');
        $validation->maxLength($_POST['email'], 40, 'email', 'email');
        
        //verifica si es un email el campo introducido con el @ y .com
        $validation->isEmail($_POST['email'], 'email');

        if($validation->getValidation()){

            $user = new UserController();
            $response['_validation'] = $validation->getValidation();

            if(isset($_POST['userId'])){
                $response['message'] = $user->updateUser($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
            }else{
                $response['message'] = $user->createUser($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
            }

            echo json_encode($response);

        }else{
            echo json_encode($validation->getErrors());

            /* version alejandro matute
            $response['validation'] = $validation->getValidation();
            $response['message'] = $validation->getErrores();    
            echo json_encode( $response);
            */
        }
    }
    else if ($_POST["action"] == "delete"){
        $user = new UserController();
        $response['message'] = $user->deleteUser($_POST["id"]);

        echo json_encode($response);
    }
?>
