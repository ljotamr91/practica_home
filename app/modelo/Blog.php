<?php

require_once("C:/wamp/www/soluciones-informaticas/core/Database.php");
   
class Blog extends Database {

    protected $_title;
    protected $_comentario;
    protected $_option;
  

    public function __construct(){
        parent::__construct();
    }

    public function getTitle(){
        return $this->_title;
    }

    public function setTitle($title){
        $this->_title = $title;
    }

    public function getComentario(){
        return $this->_comentario;
    }

    public function setComentario($comentario){
        $this->_comentario = $comentario;
    }

    public function getoption(){
        return $this->_option;
    }

    public function setOption($option){
        $this->_option = $option;
    }

    public function indexBlog(){
        
        $query =  "SELECT * 
        FROM `t_blog`";

        $stmt = $this->_pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }


    public function showBlog($id){

        $query =  "SELECT * 
        FROM `t_blog` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    public function createBlog($blog){

        try {

            $query = "insert into t_blog (title, comentario, optiones) 
            values (:title, :comentario, :optiones)";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("title", $blog['title']);
            $stmt->bindParam("comentario", $blog['comentario']);
            $stmt->bindParam("optiones", $blog['option']);
            $stmt->execute();

            $blog_id = $this->_pdo->lastInsertId();

            return "Publicacion añadida correctamente con id: ".$blog_id;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function updateBlog($blog){
        
        $query = "update `t_blog` set
        title = :title, 
        comentario = :comentario, 
        option = :option,  
        WHERE id = :id";

    $stmt = $this->_pdo->prepare($query);
    $stmt->bindParam("title", $blog['title']);
    $stmt->bindParam("comentario", $blog['comentario']);
    $stmt->bindParam("option", $blog['option']);
    $stmt->bindParam("id", $blog['id']);
    $stmt->execute();

    return "Publicacion añadida correctamente";
    }

    public function deleteBlog($id){
        $query =  "DELETE 
        FROM `t_blog` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }
}

?>