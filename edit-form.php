<div class="descripcion offset-1 col-7">	
	<h1>Actualizar</h1><br>

	<form class="admin-form" id="registration-form" action="app/request/userRequest.php">

		<div class="errors-container hidden">
			<ul class="errors"></ul>
		</div>

		<div class="success-container hidden">
			<h1 class="success"></h1>
		</div>

		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="name"  name="name" value="<?=$_POST["userName"] ?>" placeholder="Escriba su nombre">
		</div>
		
		<div class="form-group">
			<label for="apellidos">Apellidos</label>								<!-- enviamos por post el valor de value al campo userLastname revisar -->
			<input type="text" class="form-control" id="lastname"  name="lastname" value="<?=$_POST["userLastname"] ?>" placeholder="Escriba su apellido">
		</div>

		<div class="form-group">
			<label for="email">Correo</label>
			<input type="email" class="form-control" id="email"  name="email" value="<?=$_POST["userEmail"]; ?>" placeholder="Escriba su email">
		</div>

		<div class="form-group">
			<label for="password">Contraseña</label>
			<input type="password" class="form-control" id="password" name="password" value="<?=$_POST["userPassword"]; ?>" placeholder="introduzca su contraseña">
		</div>	
			
		<input type="hidden" value=<?=$_POST["userId"]; ?> id="userId" name="userId"/><!-- boton oculto que guarda el valor de la id del usuario  -->
		<!--  en $_POST se guardan todos los datos que han llegado por ajax. En este caso, desde el main.js
				se hace una llamada ajax a este archivo y se envia mediante la key userId la id de un usuario   -->
		<br><br><input type="submit" id="enviar" class="btn btn-secondary details botones slider-btn" value="Actualizar"/>
	</form>
</div>