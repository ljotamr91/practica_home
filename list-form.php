<?php 
	include("head.php"); 
	include("header.php"); 

	require_once ('C:/wamp/www/soluciones-informaticas/app/Controller/UserController.php'); 

	$user_obj = new UserController(); 
	$users = $user_obj->indexUser();
	$numberID = 0;
?>

<div>
    <div class="col-9 col-sm-9 col-md-6 col-lg-6 col-xl-12" id="contenido">
		<form id="userIndex" method="post" class="admin-form" action="app/request/userRequest.php"> 
			<table class="table table-striped">
				<thead>
					<tr>						
						<th><h5>ID</h5></th>
						<th><h5>NOMBRE</h5></th>
						<th><h5>APELLIDOS</h5></th>		
						<th><h5>EMAIL</h5></th>
						<th><h5>PASSWORD</h5></th>
						<th><h5>ELIMINAR</h5></th>
						<th><h5>MODIFICAR</h5></th>						
					</tr>

					

					<?php foreach($users as $user): ?>
						<tr>
							<td><?= $user['id'] ?></td>
							<td id=userName<?= $user['id'] ?> ><?= $user['name']; ?></td><!-- concatenamos la id del usuario con la id del campo nombre -->							
							<td	id=userLastname<?= $user['id'] ?> ><?= $user['lastname']; ?></td>
							<td	id=userEmail<?= $user['id'] ?>><?= $user['email']; ?></td>
							<td id=userPassword<?= $user['id'] ?>><?= $user['password']; ?></td>
							<td><button class="eliminar input-text" type="button" name="eliminar" value=<?=$user['id']; ?>>Eliminar</button></td>
							<td><button class="modificar" type="button" formaction="edit-form.php" value=<?=$user['id']; ?>>Modificar</button></td> <!-- La id del link/boton es igual a la id del usuario-->
						</tr>
					<?php
						endforeach 
					?>
				</thead>
			</table>
		</form>	
    </div>
</div>

<?php
	include("footer.php"); 
	include("scripts.php");
?>