<?php
	include("head.php"); 
	include("header.php"); 
?>

<div class="row descripcion">	
    <div class="offset-1 col-lg-5 col-md-12">
        <img class="imgslider" src="img/trabajador.png">		
    </div>	
    
    <div class="contenido offset-1 col-lg-4 col-sm-12">
        <h2> Mantenimiento Informatico Empresarial</h2><br>      
        <h4>
        
            Con nuestro soporte tecnico informático de Google podrás mantener tu sistema optimizado
            y con un eficaz rendimiento desde el primer día. Para ello, ofrecemos
            diferentes opciones de paquetes de mantenimiento informático para Pcs,
            Servidores y/o portátiles que incluyen prevención, implementación de sistemas 
            de seguridad informática con desplazamiento técnico y teleasistencia  
            en menos de 24 horas. Nuestro soporte informático para ordenadores y Servidores 
            ofrece además las mejores soluciones para empresas ya que permite tener todo tu sistema general informatizado.
            
        </h4>       
        <!-- boton LEER MAS -->
        <button type="button" class="btn btn-secondary details botones slider-btn"  data-toggle="tooltip" data-placement="top" title="Leer Mas">
        Leer Mas 
        </button>
    </div>					
</div>

<?php
	include("footer.php"); 
	include("scripts.php"); 
?>