<body>    
    <div class="container-fluid">
        <div class="row header">	        
            <!-- LOGO -->		
            <div class="logo col-lg-3">			                
                <h1>JOSÉ LUÍS</h1>
                <h3>Programador y<br> Maquetador Web</h3>
            </div>
        
            <!-- MENU -->                
            <div class="menu-general offset-lg-3 col-lg-5 col-sm-12">
                <div class="row">
                    <button  type="button" class="btn btn-secondary botones menu-toggle col-sm-12" id="btn-more" onclick="menu_toggle()" data-toggle="tooltip" data-placement="top" title="Menu">
                            Menu +
                    </button>

                    <div id="menu" class="hide">

                        <a href="index.php"><button  type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="top" title="Inicio">
                        Inicio
                        </button></a>
                        
                        <a href="sobre-mi.php"><button type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="right" title="Sobre Mi">
                        Sobre Mi
                        </button></a>
                        
                        <a href="slider-form.php"><button type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="top" title="Diseño">
                        Slider Form 
                        </button></a>
                        
                        <a href="blog-form.php"><button type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="right" title="blog">
                        Blog
                        </button></a>
                        
                        <a href="registratrion-form.php"><button type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="bottom" title="registro">
                        Registro
                        </button></a><br><br><br><br><br><br><br>

                         <!-- <a href="list-form.php"><button type="button" class="btn btn-secondary botones col-md-2 col-sm-12" data-toggle="tooltip" data-placement="bottom" title="registro">
                        Panel
                        </button></a> -->
                    </div>
                </div>	
            </div>	
        </div>
					
