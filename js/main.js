
//cuando carge el html ejecuta la siguiente funcion
$(document).ready(function() {

    //Carga  librería select2
    $('.select2').select2();

    //Carga de librería ckeditor
    if($('textarea').length > 0 ){
        CKEDITOR.replace('ckeditor');
    }


/*Valida el formulario y le da los valores para que el usuario rellene el formulario*/    
    $("#registration-form").validate({

        ignore: [],
        // onfocusout: true,
        wrapper: "li",
        errorClass: "is-invalid",

    //REGLAS FORMULARIO.

        rules: {
            name: {
                required: true,
              /* minlength: 3,
                maxlength: 16 */
            },
            lastname: {
                required: true,
                /* minlength: 3,
                maxlength: 16 */
            },
            email: {
                required: true,
              /*  email: true,
                maxlength: 19 */
            },
            password: {
                required: true,
               /* minlength: 8 */
            },
        // equalTo compara las contraseñas del formulario para ver si coinciden
            repetirPassword: {
                required: true,
                equalTo: "#password"
            }
        },
        // mensajes para los campos del formulario
        messages: {
            name: {
                required: "Introduzca Nombre",
               /* minlength: "El nombre debe tener un minimo de 3 caracteres",
                maxlength: "El nombre debe tener un maximo de 16 caracteres", */
            },
            lastname: {
                required: "Introduzca Apellidos",
             /*   minlength: "Apellidos debe tener un minimo de 3 caracteres",
                maxlength: "Apellidos debe tener un maximo de 16 caracteres",  */
            },
            email: {
                required: "Introduzca su correo electronico",
            /*    email: "Debe introducir un correo electronico con formato válido",
                maxlength: "Su correo electronico debe tener un maximo de 19 caracteres" */
            },
            password: {
                required: "Contraseña",
             /*   minlength: "La contraseña debe tener un minimo de 8 caracteres" */
            },
            repetirPassword: {
                required: "Tiene que poner una contraseña",
             /* minlength: "La contraseña debe tener un minimo de 8 caracteres",  */        
                equalTo: "Las Contraseña no coinciden"
            }
        }
    });
});

// FORMULARIO

// cuando haga clic en el id enviar del html ejecuta funcion event
  
$(document).on("click", "#enviar", function(event){

    // $.mockjax({
    //     url : '/foo/bar.html',
    //     isTimeout: false,
    //     responseText : 'Server Response Emulated'
        
    //   });

    // validar();    // Funcion donde ponemos las reglas y mensajes personalizados 
    
     //if($("#formulario").valid()){  //valid nos dara un Boolean 

        // Variable para los datos del texto
        event.preventDefault(); //El método preventDefault () cancela el evento si es cancelable, lo que significa que la acción predeterminada que pertenece al evento no ocurrirá.

        
        // si el textarea del html es mayor que 0 carga ckeditor
        if($('textarea').length > 0 ){
            // var textdata = CKEDITOR.instances.ckeditor.getData();
            // formData.append("comentario", textdata);

            CKEDITOR.instances.ckeditor.updateElement();
        }
//CREAR USUARIO 
        var form = "#" + $(".admin-form" ).attr('id');  //id del formulario concatenada con almoadilla.
        var url = $(form).prop("action");// del formulario cojemos la propiedad action.
        var formData =  new FormData($(form)[0]); 
        formData.append("action", "create");

        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        };

        $.ajax({
            type: 'post',
            url: url,   
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function (response) { //se ejecutara cuando la funcion tenga exito

                console.log(response);
                console.log(response._validation);
                if(response.validation){ //coge el parametro de succes y si esta bien pasa lo de dentro
                    $(".success").text(response.message);
                    $("#example-form").fadeOut();
                    $(".success-container").removeClass("hidden").fadeOut().fadeIn(); 
                }

                else{
                    $(".errors-container").removeClass("hidden");

                    $.each(response._errors, function(i, item) {
                        if(item.message){
                            $(".errors").append("<li>" + item.message + "</li>");
                            console.log(response);
                        }
                    });
                }
            },
            
            error: function(response){
                console.log(response);
            }
        }); 
    }); 


//ELIMINAR USUARIO
$(document).on('click', '.eliminar', function(e) { //al apretar el boton hace la funcion eliminar
    e.preventDefault();
    var formId = '#' + $('.admin-form').attr('id'); //Concatenamos # con la id del elemento con class=admin-form
    var url = $(formId).prop('action'); //devuelve el valor de la propiedad/atributo action
    var userId = $(this).val();
    var formData = new FormData(); //crear un formdata luego le enchufas al formdata los valores que quieras con .append y lo envias por ajax./*
    formData.append("id", userId);
    formData.append("action", "delete");
    location.reload(true); //Recarga la pagina 


    console.log(url);
	

    $.ajax({
        type: 'post',
        url: url,   //la dirreción al que llamaremos
        dataType: 'json',
        data: formData, //se almacena los datos.
        cache: false,
        contentType: false,
        processData: false,

        success: function (response) {
            $("#contenido").addClass("hidden"); //les añade el atributo hidden al id contenido
            //El método jQuery append() inserta contenido al final del elemento HTML seleccionado. response es lo que nos devuelve el servidor, php en este caso. 
            $(".title-text").append("<h1>" + response.message + "</h1>");
            console.log(response); //dentro de la consola devuleve la respuesta 
           
            //   
            // return "este usuario se elimino"
            // console.log(response);
            // location.reload(true); //Recarga la pagina
            //
        },

        error: function(response){ //si da error devuelve el valor erroneo en consola y sale una alerta
            console.log(response); 
            alert("Error");
            //location.reload(true); //Recarga la pagina completa, pero en este caso necesitamos solo la tabla
        }
    });
});


//MODIFICAR USUARIO
$(document).on('click', '.modificar', function(e) { //  evento cuando clicas en modificar 
    e.preventDefault();
    var userId = $(this).attr('value');// userId es igual al valor del atributo value de este boton que he apretado.
    var userName = $("td#userName"+userId).text(); // userName es igual seleccioname del html la etiqueta td con userName y variable userId y dame su texto.
    var userLastname = $("td#userLastname"+userId).text();
    var userEmail = $("td#userEmail"+userId).text();
    var userPassword = $("td#userPassword"+userId).text();
    /**COMENTARIO
     * 
     * var userName = $("td#userName").text();
     * 
     * Selector
     * $("td#userName")
     * seleccionamos del DOM, es decir del documento HTML,
     * la etiqueta (tag en ingles) td con la id userName.
     *  la # indica al selector que tiene que buscar una id con nombre userName
     * 
     * 
     * Funcion
     * .text()
     * funcion prestablecida de jquery que si esta vacio nos devuelve el texto que este dentro de la etiqueta
     * pero si escribimos algo dentro del parentesis estaremos asignando el texto que escribamos a la etiqueta. 
     * 
     */
    var url = $(this).attr("formaction"); //attr() nos da el valor del atributo formaction de este boton que he apretado.
    var formData = new FormData();// creamos un formdata para enviar datos por ajax en key value al servidor.
    formData.append("userId", userId);//estamos añadiendo al formdata la id del usuario seleccionado.
    formData.append("userName", userName);
    formData.append("userLastname",userLastname);
    formData.append("userEmail",userEmail);
    formData.append("userPassword",userPassword);
    

    $.ajax({
        type: 'post',
        url: url,
        data: formData,// aqui van los datos que enviamos al servidor.
        dataType: 'text',// indica el tipo de respuesta que vamos a recibir del servidor.
        cache: false,
        contentType: false,
        processData: false,// procesa los datos antes de enviarlos.

        success: function (response) {
            $("#contenido").html(response);
        },
        error: function(response){
            console.log("Ha habido un error en ajax: ");
            console.log(response);
        }
       
    });
    
    /*Primero: al pulsar el boton guardamos el valor del atributo id en una variable. LISTO
      Segundo: sustituimos el contenedor de la tabla, por el formulario de registro. LISTO
      Tercero: en el formulario debe haber un <input type="hidden" value="" id="user_id" /> LISTO
      Cuarto: añadir el valor de la variable del primer paso en el value del tercer paso LISTO
      Quinto: rellenar los input name, lastname y email del formulario con los datos de la tabla LISTO
      del usuario seleccionado y que me permita cambiarlos en la tabla. en vez de placeholder usar value LISTO
      Sexto: crear una variable por cada campo del list-form.php LISTO
      Septimo:  enviarla al value del editform LISTO
      octavo: conseguir que muestre nombres y apellidos compuestos LISTO.
      noveno : poder modificar los datos desde la pagina y que se guarden en la base de datos

    */

});