
/* ----------------------------MENU RESPONSIVE------- ------------------*/

// esta variable global sirve para abrir el menu responsive de moviles y tablets.
var open = false;

function menu_toggle() {

    if (open) {
        open = false;
        var element = document.getElementById("menu");
        element.classList.toggle("hide");
        document.getElementById("btn-more").innerHTML = "Menu +";
    } 

    else {
        open = true;
        var element = document.getElementById("menu");
        element.classList.toggle("hide");
        document.getElementById("btn-more").innerHTML = "Menu -";
    }
};







