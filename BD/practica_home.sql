-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 24-09-2018 a las 15:04:03
-- Versión del servidor: 5.7.21
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica_home`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_blog`
--

DROP TABLE IF EXISTS `t_blog`;
CREATE TABLE IF NOT EXISTS `t_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 NOT NULL,
  `comentario` text CHARACTER SET utf8 NOT NULL,
  `optiones` enum('publico','privado') CHARACTER SET utf8 NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `t_blog`
--

INSERT INTO `t_blog` (`id`, `title`, `comentario`, `optiones`, `create_at`, `update_at`, `active`) VALUES
(1, 'loren impsun', 'loren impsum loren impsum loren impsum loren impsum loren impsum loren impsum loren impsum loren impsum ', 'privado', '2018-05-07 22:00:00', '2018-05-21 22:00:00', 0),
(2, 'bonito feliz y salta', 'este tema es bonito feliz y saltarin este tema es bonito feliz y saltarineste tema es bonito feliz y saltarineste tema es bonito feliz y saltarineste tema es bonito feliz y saltarineste tema es bonito feliz y saltarineste tema es bonito feliz y saltarineste tema ', 'publico', '2018-05-14 22:00:00', '2018-05-30 22:00:00', 1),
(3, 'dfgddfgdfg', '<p>dfgdfgdgdfgdfgdf</p>\r\n', 'publico', '2018-06-27 15:03:51', '2018-06-27 15:03:51', 1),
(4, 'bitcoin la moneda', '<p>ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;ahora&nbsp;</p>\r\n', 'publico', '2018-09-24 14:47:44', '2018-09-24 14:47:44', 1),
(5, 'calculatortro', '<p>rewwerwerwerwerwqerwqerwqerwqer</p>\r\n', 'publico', '2018-09-24 14:59:43', '2018-09-24 14:59:43', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_slider`
--

DROP TABLE IF EXISTS `t_slider`;
CREATE TABLE IF NOT EXISTS `t_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_bin NOT NULL,
  `url` varchar(40) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `t_slider`
--

INSERT INTO `t_slider` (`id`, `title`, `url`, `description`, `image`, `create_at`, `update_at`, `active`) VALUES
(1, 'mis cosas', 'http://www.google.es', 'hoy en dia google conoce todas mis cosas hoy en dia google conoce todas mis cosas hoy en dia google conoce todas mis cosas hoy en dia google conoce todas mis cosas ', 'casa.jpg', '2018-05-03', '2018-05-10', 1),
(2, 'vida fuera de casa', 'www.google.es', 'hoy en dia la vida es feliz', 'casa.jpg', '2018-05-15', '2018-05-16', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_user`
--

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `t_user`
--

INSERT INTO `t_user` (`id`, `name`, `lastname`, `email`, `password`, `create_at`, `update_at`, `active`) VALUES
(1, 'Jose Luis', 'Martinez Redondo', 'Toreo@gmail.com', '$2y$10$QWZ6/On3pBdsVj18GfWjQO3egbtCvoAMwPogRVqA7Y0fgnGQ/HRV2', '2018-06-06 10:56:16', '2018-06-06 10:56:59', 1),
(2, 'jajajaja', 'ejejejejje', 'julepa@gmail.com', '$1$2rxdpQif$ia7izQf2SpV9IK3V9swot0', '2018-06-06 07:16:15', '2018-06-06 08:55:44', 1),
(3, 'josedsfds', 'wrqreqewrfdss', 'fdsfssds@fadsf.com', '$1$PTihHRFZ$lVElDIP4ea9.3hS58kEpa0', '2018-06-06 07:25:51', '2018-06-06 08:55:46', 1),
(5, 'ppo52msuyy22', 'oew22ro5uity22', 'fdi2ts52o@fd2sf.com', '$1$YMr3gQRK$gwIQ4lccIh2dZzGl3Fgpl.', '2018-06-06 07:41:40', '2018-06-06 08:55:51', 1),
(6, 'jsjsjsjsj', 'treeeee', 'jsjs@jsjsjs.com', '$2y$10$7JbvHZI4CJydrx3j8SkaauDLmP.BBhiJWbv86WbCKezkKk6vMxAfK', '2018-09-24 14:46:17', '2018-09-24 14:46:17', 1),
(7, 'cadena', 'deseres', 'cade@seres.com', '$2y$10$7Id930/I3SYNeoh3OVxUCeQqLh86ThNRwHejEwZ2E9kQAM6RG6tEW', '2018-09-24 14:59:26', '2018-09-24 14:59:26', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
