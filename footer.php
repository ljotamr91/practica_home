<!--FOOTER-->
<div class="row">	                  
    <div class="col-12 footer">					        
    <div class="row">
    
        <!-- ICONOS SOCIALES-->
        <div class="col-lg-6 col-sm-12 sociales">
            <a href="#" class="facebook fa fa-facebook"></a>
            <a href="#" class="twitter fa fa-twitter"></a>
            <a href="#" class="google fa fa-google"></a>
            <a href="#" class="linkedin fa fa-linkedin"></a>
            <a href="#" class="pinterest fa fa-pinterest"></a>				
        </div>
        
        <!--  COPYRIGHT-->					
        <div class="col-lg-5 col-sm-12 copyright">
            © Copyright 2018 diseño Web HTML CSS PHP JavaScript todos los derechos design LJOTA.
        </div>	              
    </div>										
</div>						
