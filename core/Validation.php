<?php
// Definimos la clase
    class validation {

        protected $_validation;
        protected $_errors;

        /* Hemos creado la funcion de isEmpty, es decir, está vacío(?)
        no especificamos el label ya q podemos simplificar poniendo esto porque recogerá
        cada valor segun lo que escriban en los inputs y cada label.*/
        public function __construct(){
            $this->_validation = true;
            $this->_errors = array();
        }

        public function getValidation(){
            return $this->_validation;
        }

        // devuelve todas las variables de este objeto 
        public function getErrors(){
            return get_object_vars($this);
        }

        public function isEmpty( $value, $input_id, $input_label){

            if(empty($value)){
                array_push($this->_errors, array(
                    'message' => "Complete el campo " . $input_label,
                    'id' => $input_id
                ));

                $this->_validation = false;
            }
        }

        public function minLength($value, $minLength, $input_id, $input_label){

            if(strlen($value) < $minLength){

                array_push($this->_errors, array(
                    'message'=> "El campo " . $input_label . " tiene que contener mas de " . $minLength . " carácteres",
                    'id'=> $input_id            
                ));

                $this->_validation = false;
            }
        }

        public function maxLength($value, $maxLength, $input_id, $input_label){

            if(strlen($value) > $maxLength){
                array_push($this->_errors, array(
                    'message'=> "El campo " . $input_label . " tiene que contener menos de " . $maxLength . " carácteres",
                    'id'=> $input_id            
                ));

                $this->_validation = false;
            }
        }

        public function isEmail($value, $input_id){

            if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
                array_push($this->_errors, array(
                    'message' => " Debe escribir un email válido ",
                    'id' => $input_id
                ));

                $this->_validation = false;

            }
        }
    }

?>