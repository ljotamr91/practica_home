<?php
	include("head.php"); 
	include("header.php"); 
?>

<div class="descripcion offset-1 col-7">	
	<h1>Registro</h1><br>

	<form class="admin-form" id="registration-form" action="app/request/userRequest.php">

		<div class="errors-container hidden">
			<ul class="errors"></ul>
		</div>

		<div class="success-container hidden">
			<h1 class="success"></h1>
		</div>

		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="name"  name="name" placeholder="Escriba su nombre">
		</div>
		
		<div class="form-group">
			<label for="apellidos">Apellidos</label>
			<input type="text" class="form-control" id="lastname"  name="lastname" placeholder="Escriba su apellido">
		</div>

		<div class="form-group">
			<label for="email">Correo</label>
			<input type="email" class="form-control" id="email"  name="email"  placeholder="Escriba su email">
		</div>

		<div class="form-group">
			<label for="password">Contraseña</label>
			<input type="password" class="form-control" id="password" name="password"  placeholder="introduzca su contraseña">
		</div>	
		
		<div class="form-group">
			<label for="password">Repita Contraseña</label>
			<input type="password" class="form-control" id="repetirPassword" name="repetirPassword"  placeholder="introduzca su contraseña">
		</div>	
		
		<br><br><input type="submit" id="enviar" class="btn btn-secondary details botones slider-btn" />
	</form>
</div>					


<?php
	include("footer.php"); 
	include("scripts.php");
?>